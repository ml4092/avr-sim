#!/bin/sh

set -xe

exe=main.elf

avr-gcc -mmcu=atmega328p -Os -Wl,-gc-sections -Wl,-relax -o $exe main.c
avr-objcopy -R .eeprom -O ihex $exe main.hex

