#define F_CPU 16000000
#include <avr/io.h>
#include <util/delay.h>

int main()
{
  DDRC = 0xff; // Port C is output

  while (1)
  {
    PORTC = 0xff;  // All on
    _delay_ms(100);
    PORTC = 0;    // All off
    _delay_ms(100);
  }

  return 0;
}
