# AVR Simulator

AVR simulator tool written in rust.

## Quick Start

```console
$ cargo run avr/main.hex
```

## Requirements

The build script `avr/build.sh` depends on `avr-gcc`, which comes bundled with
the [Arduino IDE](https://www.arduino.cc/en/software).

## TODO

- [x] Get raylib working
- [x] Read `.hex` files
- [ ] Interpet AVR code
- [ ] Simulate I/O
- [ ] Read more file types (`.elf`, maybe others)

## References

The font is Joystix Monospace by Raymond Larabie from [1001 Fonts](https://www.1001fonts.com/joystix-font.html#license)

