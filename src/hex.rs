use std::{fmt, iter::Peekable, str::Chars};

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum RecordType {
    Data = 0,
    Eof,
    ExtSegAddr,
    StartSegAddr,
    ExtLinAddr,
    StartLinAddr,
}

impl fmt::Display for RecordType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            Self::Data => "Data",
            Self::Eof => "EOF",
            Self::ExtSegAddr => "Extended Segment Address",
            Self::StartSegAddr => "Start Segment Address",
            Self::ExtLinAddr => "Extended Linear Address",
            Self::StartLinAddr => "Start Linear Address",
        };
        write!(f, "{s}")
    }
}

impl RecordType {
    pub fn from_u8(byte: u8) -> Option<Self> {
        match byte {
            x if x == Self::Data as u8 => Some(Self::Data),
            x if x == Self::Eof as u8 => Some(Self::Eof),
            x if x == Self::ExtSegAddr as u8 => Some(Self::ExtSegAddr),
            x if x == Self::StartSegAddr as u8 => Some(Self::StartSegAddr),
            x if x == Self::ExtLinAddr as u8 => Some(Self::ExtLinAddr),
            x if x == Self::StartLinAddr as u8 => Some(Self::StartLinAddr),
            _ => None,
        }
    }
}

#[derive(Debug)]
pub struct HexRecord {
    pub count: u8,
    pub addr: u16,
    pub kind: RecordType,
    pub data: Vec<u8>,
    pub checksum: u8,
}

impl fmt::Display for HexRecord {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{c} {k} bytes at {a}: [ ",
            c = self.count,
            k = self.kind,
            a = self.addr
        )?;
        for d in &self.data {
            write!(f, "{d:02x} ")?;
        }
        write!(f, " ] ({c:02x})", c = self.checksum)
    }
}

impl HexRecord {
    pub fn from_line(line: &str) -> Option<Self> {
        let mut chars = line.chars().peekable();

        while chars.next() != Some(':') {} // Skip up to and including colon

        //let mut sum = 0;
        let count = u8_from_chars(&mut chars)?;
        //sum = u8::wrapping_add(sum, count);

        let addr = u16_from_chars(&mut chars)?;
        //sum = u8::wrapping_add(sum, (addr >> 8) as u8 & (addr & 0xff) as u8);

        let kind = RecordType::from_u8(u8_from_chars(&mut chars).unwrap())?;
        //sum = u8::wrapping_add(sum, kind as u8);

        let mut data = Vec::new();
        for _ in 0..count {
            let byte = u8_from_chars(&mut chars)?;
            //sum = u8::wrapping_add(sum, byte);
            data.push(byte);
        }

        //println!("{s:02x}", s = -(sum as i8));

        let checksum = u8_from_chars(&mut chars)?;
        Some(Self {
            count,
            addr,
            kind,
            data,
            checksum,
        })
    }
}

fn u16_from_chars(chars: &mut Peekable<Chars>) -> Option<u16> {
    match u16::from_str_radix(&chars.take(4).collect::<String>(), 16) {
        Ok(value) => Some(value),
        Err(_) => None,
    }
}

fn u8_from_chars(chars: &mut Peekable<Chars>) -> Option<u8> {
    match u8::from_str_radix(&chars.take(2).collect::<String>(), 16) {
        Ok(value) => Some(value),
        Err(_) => None,
    }
}
