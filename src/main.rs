use raylib::{ffi::Vector2, prelude::*};
use std::path::Path;

mod hex;

use hex::*;

const FONT_SIZE: f32 = 20.0;
const PADDING: f32 = 10.0;
const APP_WIDTH: usize = 1080;
const APP_HEIGHT: usize = 720;

fn main() {
    let mut args = std::env::args().collect::<Vec<_>>();
    let prog = args.remove(0);

    if args.len() == 0 {
        print_usage_and_exit(prog);
    }

    let mut hex_path = String::new();
    let mut data = String::new();
    for arg in args {
        match arg.as_str() {
            "-h" | "--help" => print_usage_and_exit(prog),
            path if Path::new(path).exists() => {
                if Path::new(path)
                    .extension()
                    .and_then(std::ffi::OsStr::to_str)
                    == Some("hex")
                {
                    hex_path = path.to_string();
                    data = std::fs::read_to_string(path).unwrap();
                }
            }
            _ => print_usage_and_exit(prog),
        }
    }

    let records = data
        .lines()
        .map(|line| HexRecord::from_line(line).unwrap())
        .collect::<Vec<_>>();

    let (mut rl, thread) = raylib::init()
        .size(APP_WIDTH as i32, APP_HEIGHT as i32)
        .title("AVR Simulator")
        .build();
    let font = rl
        .load_font(&thread, "resources/joystix_monospace.otf")
        .unwrap();

    while !rl.window_should_close() {
        let mut d = rl.begin_drawing(&thread);

        d.clear_background(Color::WHITE);

        let mut cursor = Vector2 {
            x: PADDING,
            y: PADDING,
        };
        draw_text(&mut d, &font, &format!("{hex_path}"), cursor);

        for record in &records {
            cursor.y += FONT_SIZE;
            let addr = record.addr;
            let data = record
                .data
                .iter()
                .map(|d| format!("{d:02x} "))
                .collect::<String>();
            let kind = record.kind;

            match kind {
                RecordType::Data => {
                    draw_text(&mut d, &font, &format! {"{addr:04x}: {data}"}, cursor)
                }
                RecordType::Eof => draw_text(&mut d, &font, "EOF", cursor),
                _ => unimplemented!(),
            }
        }
    }
}

fn print_usage_and_exit(prog: String) -> ! {
    eprintln!("{prog} path/to/file.hex [-h]");
    eprintln!("Options:");
    eprintln!("    -h, --help    print this message and exit");
    std::process::exit(1);
}

fn draw_text(d: &mut RaylibDrawHandle, font: &Font, text: &str, cursor: Vector2) {
    d.draw_text_ex(font, text, cursor, FONT_SIZE, 1.0, Color::BLACK);
}
